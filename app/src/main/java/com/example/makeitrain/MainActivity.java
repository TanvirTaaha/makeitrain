package com.example.makeitrain;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

//    private Button makeItRainButton;
//    private Button showTag;
    private int moneyCounter = 0;
    private int factor;
    private TextView moneyText;
    private TextView hypeMessageView;
    private int richAmount;

    int[] colors = {
            Color.rgb(0,0,0),
            Color.rgb(255,255,255),
            Color.rgb(255,242,0),
            Color.rgb(246,230,196),
            Color.rgb(200,237,253),
            Color.rgb(252,199,202),
            Color.rgb(179,233,216),
            Color.rgb(247,233,174),
            Color.rgb(211,211,246),
            Color.rgb(108,59,57),
            Color.rgb(75,132,138),
            Color.rgb(124,96,66),
            Color.rgb(247,173,89),
            Color.rgb(235,108,63),
            Color.rgb(208,75,52),
            Color.rgb(255,202,39),
            Color.rgb(147,37,166),
            Color.rgb(22,158,250),
            Color.rgb(118,7,47),
            Color.rgb(44,183,80),
            Color.rgb(100,22,151),
            Color.rgb(88,42,71),
            Color.rgb(27,40,121),
            Color.rgb(29,112,74),
            Color.rgb(252,216,82),
            Color.rgb(247,99,64),
            Color.rgb(232,57,52)};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        moneyText = findViewById(R.id.moneyText);
        hypeMessageView = findViewById(R.id.hypeTextView);
        factor = 1;
        moneyText.setTextColor(colors[0]);

//        makeItRainButton = findViewById(R.id.buttonMakeRain);
//        makeItRainButton = findViewById(R.id.buttonMakeRain);
//        makeItRainButton.styl
//        showMoney = findViewById(R.id.buttonMakeRain);
//        showTag = findViewById(R.id.buttonShowTag);

//        showMoney.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.d("MyTag", "onClick: Show Money");
//            }
//        });

    }

    public void playAgainClick(View view) {
        moneyText.setText(R.string.initial_money_text);
        moneyCounter = 0;
        factor = 1;
        hypeMessageView.setText(null);
        moneyText.setTextColor(colors[0]);
    }
    public void makeItRainClick(View view) {

        NumberFormat toCurrency = NumberFormat.getCurrencyInstance();

        moneyCounter += 200;
        moneyText.setText(String.valueOf(toCurrency.format(moneyCounter)));


        richAmount = 5000;

        if (moneyCounter == richAmount * factor - 2000) {
            hypeMessageView.setText((factor == 1) ? R.string.about_to_be_rich_message : R.string.about_to_be_richer_message);
        }
        if (moneyCounter > richAmount * factor - 1000 && moneyCounter <= richAmount * factor) {
            String hypeMessage = String.valueOf(toCurrency.format(richAmount * factor - moneyCounter)) + " more to go!";

            hypeMessageView.setText(hypeMessage);
        }
        if (moneyCounter >= richAmount * factor) {
           hypeMessageView.setText(getResources().getString(R.string.own_message, String.valueOf(toCurrency.format(richAmount * factor))));
           factor++;
        }

        moneyText.setTextColor(colors[(factor-1) % colors.length]);
        hypeMessageView.setTextColor(colors[factor % colors.length]);
//        makeItRainButton.setBackgroundColor(colors[factor % colors.length]);
    }
}